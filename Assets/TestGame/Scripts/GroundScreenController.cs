using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(BoxCollider2D))]
public class GroundScreenController : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField][Tooltip("Use transform position of these gameObjects to spawn enemeis")] private List<GameObject> spawnPoints;
    [SerializeField][Tooltip("Prefabs of enemy variations")] private List<EnemyController> enemyVariations;

    [SerializeField][Tooltip("If player reach this point, he will win")] private bool endGround;
    #endregion

    #region work fields
    public bool active { get; private set; } = true;

    private int order = 0;
    private int currentEnemyIndex = 0;
    private List<EnemyController> enemyControllers = new List<EnemyController>();
    #endregion

    #region unity methods
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == SceneModel.playerTagConst)
        {
            if (SceneModel.curScreen_ == order) return;
            if (endGround)
            {
                SceneModel.SetAnyState(Statuses.Win);
                return;
            }
            SceneModel.SetNewGroundMain(order);
        }
    }
    #endregion

    #region public
    public void SetOrder(int or)
    {
        order = or;
    }
    public int GetOrder()
    {
        return order;
    }
    
    public void Deactivate()
    {
        gameObject.SetActive(false);
        active = false;
        foreach(EnemyController controller in enemyControllers)
        {
            Destroy(controller.gameObject);
        }
        enemyControllers = new List<EnemyController>();
    }
    public void DeactivateEnemys()
    {
        active = false;
        foreach(EnemyController controller in enemyControllers)
        {
            Destroy(controller.gameObject);
        }
        enemyControllers = new List<EnemyController>();
    }

    public void Activate()
    {
        active = true;
        gameObject.SetActive(true);
        InstaniateEnemysInSPawnPoints();
        foreach (EnemyController controller in enemyControllers)
        {
            controller.StartRun();
        }
    }
    public void Activate(bool withoutEnemy)
    {
        active = true;
        gameObject.SetActive(true);
        if (withoutEnemy) return;
        InstaniateEnemysInSPawnPoints();
        foreach (EnemyController controller in enemyControllers)
        {
            controller.StartRun();
        }
    }
    #endregion

    #region private
    private void InstaniateEnemysInSPawnPoints()
    {
        if (spawnPoints.Count > 0)
        {
            foreach (GameObject spawnPoint in spawnPoints)
            {
                // Get the current enemy controller from the list and activate it
                EnemyController enemyPrefab = enemyVariations[currentEnemyIndex];
                EnemyController newEnemy = Instantiate(enemyPrefab, spawnPoint.transform.position, Quaternion.identity);
                newEnemy.gameObject.SetActive(true);

                enemyControllers.Add(newEnemy);

                // Move to the next enemy in the list
                currentEnemyIndex++;
                if (currentEnemyIndex > enemyVariations.Count - 1)
                {
                    currentEnemyIndex -= enemyVariations.Count;
                }
            }
        }
    }
    #endregion

}
