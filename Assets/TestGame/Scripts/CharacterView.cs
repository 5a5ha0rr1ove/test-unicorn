using Spine;
using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterView : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private SkeletonAnimation charSkeletonAnimation;
    [SerializeField] private CharController charController;
    [SerializeField] private GameObject looseUI;
    [SerializeField] private GameObject winUI;
    [SerializeField] private GameObject VFXPrefab;
    #endregion

    #region Unity methods
    private void Update()
    {
        // For mobile input
        if (SceneModel.currentStatus_ == Statuses.Run)
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                RaycastHit2D hit2 = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);

                /*if (hit2.collider != null)
                {
                    if (hit2.collider.tag == SceneModel.enemyTagConst)
                    {
                        hit2.collider.gameObject.GetComponent<EnemyController>().Loose();
                    }
                }*/
                OnHit(hit2);
            }
        }
        // For mouse input
        if (Input.GetMouseButtonDown(0))
        {

            RaycastHit2D hit2 = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            /*if (hit2.collider != null)
            {
                if (hit2.collider.tag == SceneModel.enemyTagConst)
                {
                    hit2.collider.gameObject.GetComponent<EnemyController>().Loose();
                }
            }*/
            OnHit(hit2);
        }
    }
    #endregion

    #region public
    
    public void StartRunInput()
    {
        if (charSkeletonAnimation.AnimationName != SceneModel.runConst)
        {
            charSkeletonAnimation.AnimationState.SetAnimation(0, SceneModel.runConst, true);
            charController.StartRun();
        }
    }
    public void Win()
    {
        charSkeletonAnimation.AnimationState.SetAnimation(0, SceneModel.idleConst, true);
        winUI?.SetActive(true);
        looseUI?.SetActive(true);
    }
    public void Loose()
    {
        if (charSkeletonAnimation.AnimationName != SceneModel.looseConst)
        {
            charSkeletonAnimation.AnimationState.SetAnimation(0, SceneModel.looseConst, false);
            charController.Loose();
        }
        looseUI?.SetActive(true);
        
    }

    public void Restart()
    {
        SceneModel.SetAnyState(Statuses.Run);
    }

    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
    #endregion

    #region private
    private void OnHit(RaycastHit2D hit)
    {
        if (hit.collider == null) return;
        
        if (VFXPrefab != null)
        {
            GameObject newFVX = Instantiate(VFXPrefab, hit.transform.position, Quaternion.identity);
            ParticleSystem particleSystem = newFVX.GetComponent<ParticleSystem>();
            if (hit.collider != null)
            {
                newFVX.transform.position = new Vector3(newFVX.transform.position.x, newFVX.transform.position.y + hit.collider.bounds.size.y / 2);
            }
        }
        
        if (hit.collider.tag == SceneModel.enemyTagConst)
        {
            hit.collider.gameObject.GetComponent<EnemyController>().Loose();
        }
        if (hit.collider.gameObject == charController.gameObject)
        {
            Loose();
        }
    }

    private void AnimationState_Complete(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == SceneModel.shotConst)
        {
            Debug.Log(SceneModel.shotConst);
        }
        Debug.Log("AnimationState_Complete");
        charSkeletonAnimation.AnimationState.SetAnimation(0, SceneModel.runConst, true);
        charSkeletonAnimation.AnimationState.Complete -= AnimationState_Complete;
    }

    #endregion

}
