using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CapsuleCollider2D))]
public class EnemyController : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private EnemyView enemyView;

    [SerializeField][Space] private float speed = 3f;
    [SerializeField][Tooltip("Left if < 0")] private float direction = -1f;
    #endregion

    #region work fields
    private bool runNow = false;
    private bool loose = false;
    private Vector3 pos;
    private CapsuleCollider2D capsule2D;
    private Rigidbody2D body;
    #endregion

    private Vector2 offsetBoxCollider = new Vector2(-0.47f, 2.74f);
    private Vector2 sizeBoxCollider = new Vector2(2.61f, 4.1f); // set boxCollider 2d properties. Fit for this one, but can be changed

    #region Unity methods
    private void Start()
    {
        SetCapsuleAndBody();
        SceneModel.playerLoose += Win;
        SceneModel.playerWin += Loose;

    }
    private void OnDestroy()
    {
        SceneModel.playerLoose -= Win;
        SceneModel.playerWin -= Loose;
    }
    private void FixedUpdate()
    {
        if (runNow && SceneModel.currentStatus_ == Statuses.Run)
        {
            Move();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == SceneModel.playerTagConst && !loose)
        {
            CharController character = collision.gameObject.GetComponent<CharController>();
            SceneModel.SetAnyState(Statuses.Loose);
            character.Loose();
            Win();
        }
    }
    #endregion

    #region public
    public void StartRun()
    {
        runNow = true;
        enemyView.SetRun();
    }
    public void Loose()
    {
        if (!loose)
        {
            runNow = false;
            loose = true;
            enemyView.SetLoose();
            
            capsule2D.isTrigger = true;
            capsule2D.enabled = false;
            body.bodyType = RigidbodyType2D.Kinematic;
        }
    }
    public void Win()
    {
        runNow = false;
        enemyView.SetWin();
    }
    #endregion

    #region private
    private void SetCapsuleAndBody()
    {
        CapsuleCollider2D capsule = gameObject.GetComponent<CapsuleCollider2D>();
        capsule.offset = offsetBoxCollider;
        capsule.size = sizeBoxCollider;
        capsule2D = capsule;
        body = gameObject.GetComponent<Rigidbody2D>();
    }

    private void Move()
    {
        float moveDelta = direction * speed * Time.deltaTime;
        gameObject.transform.position = gameObject.transform.position + new Vector3(moveDelta, 0);

        if (pos == Vector3.zero)
        {
            pos = gameObject.transform.position;
        }

        pos = gameObject.transform.position;
    }

    #endregion

}
