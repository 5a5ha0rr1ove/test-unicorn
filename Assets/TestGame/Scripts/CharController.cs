using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class CharController : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] private CharacterView characterView;
    [SerializeField] private Transform cameraObj;
    [SerializeField] private float charSpeed = 5f;
    [SerializeField][Tooltip("Right if > 0")] private float direction = 1f;
    #endregion

    #region work fields
    private bool runNow = false;
    private Rigidbody2D charBody;
    private Vector3 pos;
    private Vector3 startRunPos;
    #endregion

    #region Unity methods
    private void Start()
    {
        SceneModel.playerRestart += Restart;
        SceneModel.playerWin += Win;
    }
    private void OnDestroy()
    {
        SceneModel.playerRestart -= Restart;
        SceneModel.playerWin -= Win;
    }
    private void FixedUpdate()
    {
        if (runNow)
        {
            Move();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log(collision.gameObject.name);

        /*GroundScreenController groundScreenController;
        if (collision.gameObject.TryGetComponent(out groundScreenController))
        {
            if (SceneModel.curScreen_ == groundScreenController.order) return;
            SceneModel.SetNewGroundMain(groundScreenController.order);
        }*/
    }
    #endregion

    #region public methods
    public void StartRun()
    {
        SceneModel.SetAnyState(Statuses.Run);
        runNow = true;
        startRunPos = transform.position;
    }
    public void StopRun()
    {
        SceneModel.SetAnyState(Statuses.Idle);
        runNow = false;
    }
    public void Loose()
    {
        SceneModel.SetAnyState(Statuses.Loose);
        characterView.Loose();
        runNow = false;
    }
    #endregion

    #region private
    private void Move()
    {
        SetCharBody();

        float moveDelta = direction * charSpeed * Time.deltaTime;
        
        gameObject.transform.position = gameObject.transform.position + new Vector3(moveDelta, 0);

        
        if (pos == Vector3.zero)
        {
            pos = gameObject.transform.position;
        }
        float cameraMove = 0f;
        if (direction > 0)
        {
            cameraMove = gameObject.transform.position.x - pos.x;
        }
        else
        {
            cameraMove = pos.x - gameObject.transform.position.x;
        }
        cameraObj.position += new Vector3(cameraMove, 0);


        pos = gameObject.transform.position;
    }
    private void Restart()
    {
        runNow = true;
        characterView.StartRunInput();
        gameObject.transform.position = startRunPos;
    }
    private void Win()
    {
        runNow = false;
        characterView.Win();
    }

    private void SetCharBody()
    {
        if (charBody == null)
        {
            charBody = gameObject.GetComponent<Rigidbody2D>();
        }
        if (charBody == null)
        {
            Debug.Log("Cannot get Rigidbody2D from char gameObject");
        }
    }
    #endregion

}
