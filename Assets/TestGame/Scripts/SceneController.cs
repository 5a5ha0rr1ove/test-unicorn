using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.VisualScripting;

/// <summary>
/// This controller is used to create and manage the areas (screens) of land from which the road for the flat level is created.
/// </summary>
public class SceneController : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField][Tooltip("Ground prefab that will be created on the start of the level")] private GroundScreenController firstGroundScreen;
    [SerializeField][Tooltip("Ground prefab that will be created on the end of the level")] private GroundScreenController lastGroundScreen;
    [SerializeField][Tooltip("A list of prefabs of land variations that will be created at the start or in the editor.")] private List<GroundScreenController> screensVariations = new List<GroundScreenController>();
    [SerializeField][Tooltip("Sky object that will be stretched to the width of the entire playing field")] private GameObject SkyObject;
    [SerializeField][Tooltip("Point for first prefab")][Space] private Vector3 startPoint; // Vector3(-38.0400009,-6.48000002,0)
    [SerializeField] private int screensToDisplay = 2;
    [SerializeField][Tooltip("Decides which variations will be created, based on randomness or simply by serial number in the list")] private bool random = true;
    #endregion

    #region work fields
    private int displayedScrens = 0;
    private float screenOffsetRight = 0f;
    private float endPoint = 0f;
    #endregion

    #region unity methods
    private void Start()
    {
        CreateModularScreens();
        SceneModel.activeNewGround += HandleNewGround;
        SceneModel.playerRestart += Restart;
    }
    private void OnDestroy()
    {
        SceneModel.activeNewGround -= HandleNewGround;
        SceneModel.playerRestart -= Restart;
    }
    #endregion

    #region public methods
    public void CreateModularScreens()
    {
        int firstAndLast = 0;
        if (firstGroundScreen != null) { firstAndLast++; }
        if (lastGroundScreen != null) { firstAndLast++; }
        if (transform.childCount != screensToDisplay + firstAndLast)
        {
            Debug.Log("Rebuild");
            displayedScrens = 0; // reset fields that can be already have values from editor
            screenOffsetRight = 0;

            #if UNITY_EDITOR
                CleanUpChildsEditor();
            #else
                CleanUpChilds();
            #endif

            List<GroundScreenController> createdGrounds = new List<GroundScreenController>();

            if (firstGroundScreen != null)
            {
                var screenVar = Instantiate(firstGroundScreen, transform.position, Quaternion.identity);
                screenVar.transform.parent = transform;
                SetAllParametressInGround(screenVar.gameObject, createdGrounds);
            }
            for (int i = 0; i < screensToDisplay; i++)
            {
                GameObject screenVar = DefineGroundScreen();
                SetAllParametressInGround(screenVar, createdGrounds);
            }
            if (lastGroundScreen != null)
            {
                var screenVar = Instantiate(lastGroundScreen, transform.position, Quaternion.identity);
                screenVar.transform.parent = transform;
                SetAllParametressInGround(screenVar.gameObject, createdGrounds);
            }

            endPoint = createdGrounds[createdGrounds.Count - 1].transform.position.x;

            SpriteRenderer skyRenderer = SkyObject.GetComponent<SpriteRenderer>();
            float skyPosX = (startPoint.x + endPoint) / 2;
            skyRenderer.size = new Vector2(screenOffsetRight, skyRenderer.size.y);
            skyRenderer.gameObject.transform.position = new Vector3(skyPosX, skyRenderer.gameObject.transform.position.y);

            SceneModel.SetGroundsControllers(createdGrounds);
        }
        else
        {
            ResetOrder();
        }
    }



    public void CleanUpChilds()
    {
        screenOffsetRight = 0;
        displayedScrens = 0;
        int childs = transform.childCount;
        if (childs > 0)
        {
            List<GameObject> childss = GetAllChilds();
            foreach (GameObject child in childss)
            {
                Destroy(child);
            }
        }
    }
    /// <summary>
    /// Must be called only from editor!
    /// </summary>
    public void CleanUpChildsEditor()
    {
        screenOffsetRight = 0;
        displayedScrens = 0;
        int childs = transform.childCount;
        if (childs > 0)
        {
            List<GameObject> childss = GetAllChilds();
            foreach (GameObject child in childss)
            {
                DestroyImmediate(child);
            }
        }
    }
    #endregion

    #region private
    /// <summary>
    /// decides which screen will be created, based on randomness or simply by serial number
    /// </summary>
    /// <returns></returns>
    private GameObject DefineGroundScreen()
    {
        if (random)
        {
            int randScreen = UnityEngine.Random.Range(0, screensVariations.Count);
            GameObject screenVar = Instantiate(screensVariations[randScreen].gameObject, gameObject.transform);
            return screenVar;
        }
        else
        {
            if (screensVariations.Count - 1 >= displayedScrens)
            {
                GameObject screenVar = Instantiate(screensVariations[displayedScrens].gameObject, gameObject.transform);
                return screenVar;
            }
            int displ = displayedScrens;
            while (screensVariations.Count <= displ)
            {
                displ -= screensVariations.Count;
            }
            GameObject screenVarr = Instantiate(screensVariations[displ].gameObject, gameObject.transform);
            return screenVarr;
        }
    }
    
    private void ResetOrder()
    {
        if (SceneModel.groundControllers_.Count != 0)
        {
            int seted = 0;
            foreach (GroundScreenController screenVar in SceneModel.groundControllers_)
            {
                screenVar.SetOrder(seted);
                seted++;
            }
        }
        else
        {
            int children = transform.childCount;
            List<GroundScreenController> childs = new List<GroundScreenController>();
            for (int i = 0; i < children; ++i)
            {
                //print("For loop: " + transform.GetChild(i));
                //childs.Add(gameObject.transform.GetChild(i).gameObject);
                GroundScreenController groundScreenController;
                if (gameObject.transform.GetChild(i).TryGetComponent(out groundScreenController))
                {
                    //Debug.Log("Getted " + seted);
                    groundScreenController.SetOrder(i);
                    childs.Add(groundScreenController);
                }
            }
            SceneModel.SetGroundsControllers(childs);
        }
    }
    private void HandleNewGround(int number)
    {
        List<GroundScreenController> grounds = SceneModel.groundControllers_;
        if (grounds.Count == 0)
        {
            grounds = GetAllGroundControllers();
        }
        for (int num = 0; num < grounds.Count; num++)
        {
            if (num <= number - SceneModel.displayAroundPlayer || num >= number + SceneModel.displayAroundPlayer)
            {
                if (grounds[num].active)
                {
                    grounds[num].Deactivate();
                }
            }
            else
            {
                if (!grounds[num].active)
                {
                    grounds[num].Activate();
                }
            }
        }
    }
    private void HandleNewGround(int number, int startOffset)
    {
        Debug.Log("HandleNewGround offset");
        List<GroundScreenController> grounds = SceneModel.groundControllers_;
        if (grounds.Count == 0)
        {
            grounds = GetAllGroundControllers();
        }
        for (int num = 0; num < grounds.Count; num++)
        {
            if (num <= number - SceneModel.displayAroundPlayer || num >= number + SceneModel.displayAroundPlayer)
            {
                grounds[num].Deactivate();
                Debug.Log("Deactivate " + num);
                /*if (grounds[num].active)
                {
                    grounds[num].Deactivate();
                    Debug.Log("Deactivate " + num);
                }*/
            }
            else
            {
                if (!grounds[num].active)
                {
                    if (num <= startOffset)
                    {
                        grounds[num].Activate(withoutEnemy: true);
                    }
                    else
                    {
                        grounds[num].Activate();
                    }
                }
            }
        }
    }

    private List<GameObject> GetAllChilds()
    {
        int children = transform.childCount;
        List<GameObject> childs = new List<GameObject>();
        for (int i = 0; i < children; ++i)
        {
            childs.Add(gameObject.transform.GetChild(i).gameObject);
        }
        return childs;
    }
    private List<GroundScreenController> GetAllGroundControllers()
    {
        int children = transform.childCount;
        List<GroundScreenController> groundControllers = new List<GroundScreenController>();
        for (int i = 0; i < children; ++i)
        {
            groundControllers.Add(gameObject.transform.GetChild(i).gameObject.GetComponent<GroundScreenController>());
        }
        return groundControllers;
    }

    private void SetAllParametressInGround(GameObject screenVar, List<GroundScreenController> createdGrounds)
    {
        screenVar.gameObject.SetActive(true);
        screenVar.name += " (" + displayedScrens.ToString() + ")";
        screenVar.transform.position = startPoint;
        Vector3 screenVarSize = Vector3.zero;

        // Get size from boxCollider
        screenVarSize = screenVar.GetComponent<BoxCollider2D>().size;

        screenVar.transform.position += new Vector3(screenOffsetRight, 0); // move to the right by the width of the already created screens

        GroundScreenController groundScreenController;
        if (screenVar.TryGetComponent(out groundScreenController))
        {
            groundScreenController.SetOrder(displayedScrens);
            createdGrounds.Add(groundScreenController);
        }

        displayedScrens++;
        screenOffsetRight += screenVarSize.x;
    }

    private void Restart()
    {
        foreach (GroundScreenController groundController in SceneModel.groundControllers_)
        {
            groundController.DeactivateEnemys();
        }
        HandleNewGround(0, SceneModel.startOffset); // start from the begining
    }
    #endregion

}
