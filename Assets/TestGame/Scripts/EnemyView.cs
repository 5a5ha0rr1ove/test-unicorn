using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(SkeletonAnimation))]
public class EnemyView : MonoBehaviour
{
    
    [SerializeField] private SkeletonAnimation skeletonAnimation;
    private bool runNow = false;

    #region public
    public void SetLoose()
    {
        runNow = false;
        ResetDummyAnimator();
        skeletonAnimation.AnimationState.SetAnimation(0, SceneModel.angryConst, true);
    }
    public void SetRun()
    {
        if (!runNow)
        {
            ResetDummyAnimator();
            skeletonAnimation.AnimationState.SetAnimation(0, SceneModel.runConst, true);
            runNow = true;
        }
    }
    public void SetWin()
    {
        runNow = false;
        ResetDummyAnimator();
        skeletonAnimation.AnimationState.SetAnimation(0, SceneModel.winConst, true);
    }
    #endregion

    private void ResetDummyAnimator()
    {
        if (skeletonAnimation == null)
        {
            skeletonAnimation = gameObject.GetComponent<SkeletonAnimation>();
            if (skeletonAnimation == null)
            {
                Debug.Log("Cannot acces skeletonAnimation!");
            }
        }
        
    }

}
