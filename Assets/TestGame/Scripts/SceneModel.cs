using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneModel
{
    #region properties
    public static Statuses currentStatus_ { get { return currentStat; } }
    private static Statuses currentStat = Statuses.Idle;
    
    public static float maxScreens_ { get { return maxScreens; } }
    private static float maxScreens = 3;
    
    public static int curScreen_ { get { return curScreen; } }
    private static int curScreen = -1;

    public static List<GroundScreenController> groundControllers_ { get { return groundControllers; } }
    private static List<GroundScreenController> groundControllers = new List<GroundScreenController>();
    #endregion

    #region events
    public delegate void activeNewGroundDel(int groundNum);
    public static event activeNewGroundDel activeNewGround;
    
    public delegate void loose();
    public static event loose playerLoose;
    public delegate void restart();
    public static event restart playerRestart;
    public delegate void win();
    public static event win playerWin;
    #endregion

    #region const
    public const int displayAroundPlayer = 2;
    public const int startOffset = 2;
    public const string playerTagConst = "Player";
    public const string enemyTagConst = "Enemy";

    public const string runConst = "run";
    public const string idleConst = "idle";
    public const string shotConst = "shoot";
    public const string looseConst = "loose";
    public const string angryConst = "angry";
    public const string winConst = "win";
    #endregion

    public static void ApplicationEnd()
    {
        currentStat = Statuses.End;
    }

    public static void ApplicationRun()
    {
        currentStat = Statuses.Run;
    }


    public static void SetAnyState(Statuses status)
    {
        if (status == Statuses.Loose && currentStat != status)
        {
            playerLoose?.Invoke();
        }
        if (status == Statuses.Run && (currentStat == Statuses.Loose || currentStat == Statuses.Win))
        {
            currentStat = status;
            curScreen = 0;
            playerRestart?.Invoke();
        }
        if (status == Statuses.Win && currentStat != status)
        {
            currentStat = status;
            playerWin?.Invoke();
        }

        currentStat = status;
    }

    public static void SetGroundsControllers(List<GroundScreenController> screens)
    {
        maxScreens = screens.Count;
        groundControllers = screens;
    }
    public static void SetNewGroundMain(int newMainGround)
    {
        curScreen = newMainGround;
        activeNewGround?.Invoke(newMainGround);
    }
    
}

public enum Statuses
{
    Idle,
    Run,
    End,
    Loose,
    Win
}