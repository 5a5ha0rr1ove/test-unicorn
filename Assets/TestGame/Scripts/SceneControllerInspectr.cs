using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneController))]
public class SceneControllerInspectr : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SceneController sceneController = (SceneController)target;
        if (GUILayout.Button("Instaniate level"))
        {
            sceneController.CreateModularScreens();
        }
        if (GUILayout.Button("Cleanup level"))
        {
            sceneController.CleanUpChildsEditor();
        }
    }
}
