# Test-unicorn-runner

### Выполненное тестовое задание

![Example Start Screen](/Assets/TestGame/Examples/test-unicornStart.PNG)
![Example Game](/Assets/TestGame/Examples/test-unicornGame.PNG)
![Example Fail](/Assets/TestGame/Examples/test-unicornFail.PNG)
![Example Win](/Assets/TestGame/Examples/test-unicornWin.PNG)


### Исходное задание:

#### Механика:

- Персонаж (Player) автоматически идет по локации (без управления игроком). Ему навстречу бегут противники (Enemy).
- При клике по противнику Персонаж стреляет в него и противник уничтожается.
- Если противник добежал до Персонажа, то Персонаж проигрывает.
- Если Персонаж дошел до конца локации, он побеждает.

#### Сеттинг:

- Персонаж - единорог с футуристичным электроружьем
- Противник - антропоморфная тыква
- Окружение - фермерские поля и/или кладбище (необходимые ассеты представлены)
<br>
Необходимо собрать визуально красивую (на ваш вкус) сцену игрового уровня из представленных ассетов и добавить скрипты, реализующие механику игры.  
<br>
Все необходимые ассеты и библиотеки уже добавлены в проект.
<br>
Персонажи анимированы с помощью Spine, для их работы в Unity используется библиотека spine-unity runtime, которая уже добавлена в заготовку проекта. Документация с описанием API представлена здесь: http://esotericsoftware.com/spine-unity
<br>
Разрешается использовать сторонние ассеты (изображения, звуки, эффекты), но их стилистическая уместность в игре будет учитываться при оценке тестового задания.
<br>

##### Версия Unity, используемая в проекте: 2021.3.12f1
<br>

### Что было добавлено от себя:
<br>

#### Генератор уровней
![Example Generator](/Assets/TestGame/Examples/sceneControllerInspectr.PNG)
<br>
Для упрощения создания визуально приятных уровней, и демонстрации моего умения писать расширения для эдитора, был добавлен генератор. Он создаёт из префабов участков земли плоский уровень, может рандомно миксовать префабы, или же идти прямо по списку.
<br>
На префабах должен быть скрипт GroundScreenController, у него есть три сериализуемых эдитором поля:
1. List<GameObject> spawnPoints - использует transform.position объектов из этого списка для спавна на этих позициях врагов.
2. List<EnemyController> enemyVariations - список префабов противников, на которых должен быть скрипт EnemyController.
3. bool endGround - если True, то игрок выиграет достигнув этой точки.
<br>
У самого SceneController есть следующие поля:

1. GroundScreenController firstGroundScreen - если поле не пусто, то этот префаб всегда будет в начале уровня.
2. GroundScreenController lastGroundScreen - если поле не пусто, то этот префаб всегда будет в конце уровня.
3. List<GroundScreenController> screensVariations - список вариантов префабов земли, которые заполняют пространство между firstGroundScreen и lastGroundScreen. 
4. GameObject SkyObject - объект, который будет растянут на ширину всего уровня.
5. Vector3 startPoint - точка, на которой сгенерируется первый префаб земли.
6. int screensToDisplay - колличество префабов, которые будут созданы между firstGroundScreen и lastGroundScreen. Можно менять и получать разную длину уровня.
7. bool random - Если true, то во время создания земли между firstGroundScreen и lastGroundScreen будет браться случайный префаб из screensVariations. Если false, то идёт по списку screensVariations от первого к последнему, достигнув конца начинает с начала. Получаются зацикленные локации.
<br>

Кнопки "Instaniate level" и "Cleanup level" соответственно генерируют и удаляют уровни.

#### Результат генерации
![Example Generator](/Assets/TestGame/Examples/sceneControllerResult1.PNG)
![Example Generator](/Assets/TestGame/Examples/sceneControllerResult2.PNG)
